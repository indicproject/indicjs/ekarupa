const chai = require('chai');
const {
  asciiToUnicode: a2u
} = require('../dist/ekarupa.umd.js');
const {
  ambili: map
} = require('unicode-conversion-maps');

chai.should();

const ZWNJ = '\u200C'

describe('ekarupa', () => {
  it('should convert kartika to unicode properly', () => {
    a2u('A', map).should.equal('അ');

    a2u('At]£ ka¿∏nt°-≠Xv', map).should.equal('അപേക്ഷ സമര്‍പ്പിക്കേണ്ടത്' + ZWNJ);

    a2u('DtZym-Kÿ-≥', map).should.equal('ഉദ്യോഗസ്ഥന്‍');
    
  });
  // it('should convert some other font to unicode properly', () => {
  // });
});
