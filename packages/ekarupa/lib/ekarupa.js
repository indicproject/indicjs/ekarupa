function getDefaultOptionsFromMap(map) {
  return {
    LHSGreatestWidth: calculateLHSGreatestWidth(map.map)
  }
}

export function asciiToUnicode(string, usermap, useroptions) {
  const map = preprocessMap(usermap);
  const defaultOptions = getDefaultOptionsFromMap(map);
  const options = {
    ...defaultOptions,
    ...useroptions
  }
  
  var converted = '';
  var currIndex = 0;
  var currentDecodedChar = '';
  var currentOriginalChar = '';
  processingLoop:
    while (currIndex < string.length) {
      currentOriginalChar = currentDecodedChar = string.slice(currIndex, currIndex + 1);
      for (let currLHSWidth = options.LHSGreatestWidth; currLHSWidth > 0; currLHSWidth--) {
        if (map.map[string.slice(currIndex, currIndex + currLHSWidth)] !== undefined) {
          currentDecodedChar = map.map[string.slice(currIndex, currIndex + currLHSWidth)];
          converted += currentDecodedChar;
          currIndex += currLHSWidth;
          // console.log(currentDecodedChar);
          continue processingLoop;
        }
      }
      converted += currentDecodedChar;
      currIndex += 1;
    }
  var postProcessed = '';
  currIndex = 0;
  var currChar = '';
  var baseFound = false;
  var joiningHold = false;
  var prebaseBuffer = '';
  var baseBuffer = '';
  while (currIndex < converted.length) {
    currChar = converted.slice(currIndex, currIndex + 1);
    if (joiningHold) {
      currChar = joiningHold + currChar;
      joiningHold = false;
    }
    if (map.prebase.includes(currChar)) {
      if (baseFound) {
        postProcessed += prebaseBuffer;
        prebaseBuffer = '';
        baseFound = false;
      }
      prebaseBuffer += currChar;
    } else if (map.joiner.includes(currChar)) {
      joiningHold = currChar;
      // postProcessed += currChar;
      baseFound = false;
    } else if (map.postbase.includes(currChar)) {
      postProcessed += prebaseBuffer;
      prebaseBuffer = '';
      baseFound = false;
      postProcessed += currChar;
    } else {
      if (baseFound === false) {
        postProcessed += currChar;
        baseFound = true;
      } else {
        postProcessed += prebaseBuffer;
        prebaseBuffer = '';
        postProcessed += currChar;
      }
    }
    currIndex += 1;
  }
  for (let rule of Object.keys(map.composition)) {
    postProcessed = postProcessed.replaceAll(rule, map.composition[rule])
  }
  return postProcessed;
}

const defaultMapOptions = {
  prebase: ' േ െ ൈ ്ര'.split(' '),
  postbase: 'ാ ി ീ ു ൂ ഃ'.split(' '),
  joiner: '്'.split(' '),
  composition: {
    'ോ': 'ോ',
    'ൊ': 'ൊ'
  }
}

function preprocessMap(config) {
  let result = {
    ...defaultMapOptions,
    ...config
  };

  if (typeof config.map !== "object") {
    throw "No mapping passed in"
  } else {
    result.map = processMap(config.map)
  }
}

function processMap(map) {
  let result = {}
  for (const rule of Object.keys(map)) {
    result[rule.trim()] = map[rule].trim();
  }
  result[' '] = ' ';
  result['\n'] = '\n';
  return result
}

String.prototype.replaceAll = function (search, replacement) {
  var target = this;
  return target.replace(new RegExp(search, 'g'), replacement);
};

function calculateLHSGreatestWidth(map) {
  return calculateGreatestWidth(map, 'lhs');
}

function calculateRHSGreatestWidth(map) {
  return calculateGreatestWidth(map, 'rhs');
}

function calculateGreatestWidth(map, index) {
  let greatestWidth = 0;
  for (const key in Object.keys(map)) {
    const width = getWidthOf(key, map, index);
    if (width > greatestWidth) {
      greatestWidth = width;
    }
  }
  return greatestWidth;
}

function getWidthOf(key, map, side) {
  if (side === 'lhs') return key.length;
  if (side === 'rhs') return map[key].length;
  throw "Invalid side (neither lhs nor rhs)";
}
