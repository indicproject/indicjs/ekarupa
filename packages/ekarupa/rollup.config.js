const pkg = require('./package.json');

export default {
  input: 'lib/ekarupa.js',
  output: [
    {
      file: pkg.main,
      format: 'umd',
      name: 'ekarupa',
      sourcemap: 'true'
    },
    {
      file: pkg.module,
      format: 'es',
      sourcemap: 'true'
    }
  ]
}
