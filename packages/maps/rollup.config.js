let pkg = require('./package.json');

export default {
  input: 'lib/maps.js',
  output: [
    {
      file: pkg.main,
      format: 'umd',
      name: 'unicode-conversion-maps',
      sourcemap: true
    },
    {
      file: pkg.module,
      format: 'es',
      sourcemap: true
    }
  ]
}
